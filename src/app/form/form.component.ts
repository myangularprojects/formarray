import { Component } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
form:any;

constructor(private fb:FormBuilder){
  this.form=this.fb.group({
    firstName:[''],
    lastName:[''],
    contact:[''],
    email:[''],
    educationalDetails:this.fb.array([
        
     this.fb.group({
        degreeName:[''],
        type:[''],
        passingYear:[''],
        marksObtained:['']
        })
        
    ]),
    address:this.fb.group({
          city:[''],
          state:[''],
          country:[''],
          pincode:['']
    })
})
}

addForm(){
  this.getEducationalDetails().push( 
    this.fb.group({
      degreeName:[''],
      type:[''],
      passingYear:[''],
      marksObtained:['']
      })
      )
}

removeForm(index:any){
  this.getEducationalDetails().removeAt(index);
}

getEducationalDetails(){
  return this.form.get("educationalDetails") as FormArray;
}


public submit(){
  console.log(this.form)
}
}
